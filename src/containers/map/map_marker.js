import React, { Component } from 'react';

import { connect } from 'react-redux';
import { DEFAULT_ZOOM } from './map_detail.js';

const MARKER_SIZE_FACTOR = 10;
const DEFAULT_MARKER_SIZE = MARKER_SIZE_FACTOR * DEFAULT_ZOOM;

class MapMarker extends Component{
	constructor(props){
		super(props);
		this.state = {
			markerImg:'../../resources/default.png'
		}

		this.changeMarkerSize = this.changeMarkerSize.bind(this);
	}


	changeMarkerSize(size){
		/*if(size > 10){
			this.setState({markerImg:'../../resources/marker_green.png'})
			return;
		}*/
		if(Number.isNaN(size)){
			return DEFAULT_MARKER_SIZE;
		}else{
			console.log(`current marker size : ${MARKER_SIZE_FACTOR * size}`);
			return MARKER_SIZE_FACTOR * size;
		}
	}

	render(){

		return(
				<div className='markerHolder'>
					<img
					className='markerImg'
					src={this.state.markerImg}
					width={this.changeMarkerSize(this.props.changedMapSize)}
					height={this.changeMarkerSize(this.props.changedMapSize)} />
				</div>
			)
	}
}



function mapStateToProps(state){
	return {
		changedMapSize:state.changedMapSize
	}
}


export default connect(mapStateToProps)(MapMarker);