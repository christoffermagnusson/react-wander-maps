import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GoogleMapReact from 'google-map-react';
import { GoogleMap, withGoogleMap } from 'google-map-react';
import MapMarker from './map_marker.js';
import { changeMapSize } from '../../actions/index.js';

const DEFAULT_PROPS = {lat:57.7006827,lng:11.6136629}
export const DEFAULT_ZOOM = 5;

class MapDetail extends Component{
	constructor(props){
		super(props);

		this.renderMarkers 	  = this.renderMarkers.bind(this);
		this.zoomLevelChanged = this.zoomLevelChanged.bind(this);
		this.encounterFocused = this.encounterFocused.bind(this);
	}


	renderMarkers(coordinates){
		const Holder = () => <div></div>;
		if(coordinates.length == 0){
			console.log('No coordinates found');
			return <Holder />;
		}


		return coordinates.map(c => <MapMarker
				key={`${c.lng}:${c.lat}`}
				lng={c.lng}
				lat={c.lat} />
		)
	}

	renderEncounters(encounters){
		const Holder = () => <div></div>;
		if(encounters.length <= 1){ // 1 is allowed because the initial msg is contained within an array with 1 element
			console.log('No encounters found');
			return <Holder />;
		}


		return encounters.map(encounter => <MapMarker
				key={`${encounter.coordinates.lng}:${encounter.coordinates.lat}`}
				lng={encounter.coordinates.lng}
				lat={encounter.coordinates.lat} />
				)
	}




	zoomLevelChanged(map){
		console.log(map.zoom);
		this.props.changeMapSize(map.zoom);
		console.log(`current focus ${this.props.focusedEncounter} `);
	}

	encounterFocused(encounter){
		if(encounter === null || !encounter.hasOwnProperty('coordinates')){ // to check whether initial msg should be displayed
			console.log('Undefined encounter, not changing focus')
			return;
		}
		console.log(`Encounter focused at : lat ${encounter.coordinates.lat} lng ${encounter.coordinates.lng}`);
		return {
			lat:encounter.coordinates.lat,
			lng:encounter.coordinates.lng
		}
	}

	render(){
		return(
				<div className='google-map' style={{ height: '80vh', width: '100%'}}>
					<GoogleMapReact
						id='map'
						onChange={this.zoomLevelChanged}
						bootstrapURLKeys={{ key:'AIzaSyBXHEh9viL4kERXFwdizVdXnTsDp6ka8d4' }}
						defaultCenter={DEFAULT_PROPS}
						defaultZoom={DEFAULT_ZOOM}
						center={this.encounterFocused(this.props.focusedEncounter)}>
						{this.renderEncounters(this.props.encounters)}
					</GoogleMapReact>
				</div>
			)
	}

}

/*
getCoord(){
	if(! navigator.geolocation){
		console.log('geolocation not supported');
		return;
	}
	navigator.geolocation.getCurrentPosition(pos => {
		this.setState({
			coord: pos
		});
	}, err => console.log(err));
}
*/
function mapStateToProps(state){
	return {
		encounters:state.encounters,
		focusedEncounter:state.focusedEncounter
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({changeMapSize:changeMapSize}, dispatch);
}



export default connect(mapStateToProps, mapDispatchToProps)(MapDetail);
