import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { focusMapOnEncounter } from '../../actions/index.js';
import styles from '../../../style/encounter_list_item.css.js';
import { Card, CardActionArea, CardContent, CardMedia, Typography} from '@material-ui/core';


const ListItem = ({ value, lat, lng, monsterName, monsterHealth, monsterAtk }) => {
	value = value === 'undefined' ? 'Updating...' : value;
	return <Card style={styles.card} elevation={0}>
					<CardActionArea>
						<CardMedia
							component='img'
							style={styles.img}
							height='200'
							image='../../resources/placeholder.png'
						/>
					  <CardContent>
							<Typography variant='h5' component='h2'>
								Monster
							</Typography>
							<Typography variant='p' style={styles.desc}>
								{`Name: ${monsterName}`}
							</Typography>
							<Typography variant='p' style={styles.desc}>
								{`Health: ${monsterHealth}`}
							</Typography>
							<Typography variant='p' style={styles.desc}>
								{`Attack power: ${monsterAtk}`}
							</Typography>
						</CardContent>
					</CardActionArea>
				</Card>

}


class EncounterListItem extends Component{
	constructor(props){
		super(props);
		this.focusMapOn = this.focusMapOn.bind(this);
	}

	focusMapOn(encounter){
		this.props.focusMapOnEncounter(encounter);
	}


	render(){
		return (
				<div
		 			 key={`${this.props.lat}:${this.props.lng}`}
		 			 onClick={event => {
		 				event.preventDefault();
		 				this.focusMapOn(this.props.encounter);
		 				}}
					>
			 		<ListItem
				 		key={`${this.props.lat}:${this.props.lng}`}
				 		lat={this.props.lat}
				 		lng={this.props.lng}
				 		value={`lat:${this.props.lat} lng:${this.props.lng}`}
				 		monsterName={this.props.monsterName}
				 		monsterHealth={this.props.monsterHealth}
				 		monsterAtk={this.props.monsterAtk}>
			 		</ListItem>
		 		</div>

			)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({focusMapOnEncounter:focusMapOnEncounter}, dispatch);
}


export default connect(null, mapDispatchToProps)(EncounterListItem);
