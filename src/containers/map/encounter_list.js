import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid, Row, Col } from 'react-flexbox-grid';
import EncounterListItem from './encounter_list_item.js';
import { focusMapOnEncounter } from '../../actions/index.js';
import styles from '../../../style/encounter_list.css.js';
import { List, ListItem, ListItemText, Avatar, Slide } from '@material-ui/core';
import ImageIcon from '@material-ui/icons/Image';
const COLUMN_KEY = 'COLUMN_KEY';

class EncounterList extends Component{
	constructor(props){
		super(props);
		this.state = {
			activeItem: 0,
		}

		this.focusMapOn = this.focusMapOn.bind(this);
		this.renderActiveItem = this.renderActiveItem.bind(this);
		this.renderListItems 	= this.renderListItems.bind(this);
	}

	focusMapOn(encounter){
		this.props.focusMapOnEncounter(encounter);
	}

	renderActiveItem(encounter){
		console.log(encounter);
		if(!encounter.hasOwnProperty('coordinates')){
			console.log('not having coordinates prop. defaulting to msg component');
			return <ListItem button>
			 				<ListItemText primary={encounter.msg} />
						 </ListItem>

			// <div key={encounter.msg} className='list-group-item' style={styles.listGroupItem}>{encounter.msg}</div>;
		}
		let lat 			= encounter.coordinates.lat;
		let lng 			= encounter.coordinates.lng;
		let monsterName 	= encounter.enemy.name;
		let monsterHealth 	= encounter.enemy.health;
		let monsterAtk		= encounter.enemy.attackPower;

		 return <Slide direction='right' in={true} timeout={750}>
							<EncounterListItem
					 		encounter={encounter}
					 		key={`${lat}:${lng}`}
					 		lat={lat}
					 		lng={lng}
					 		value={`lat:${lat} lng:${lng}`}
					 		monsterName={monsterName}
					 		monsterHealth={monsterHealth}
					 		monsterAtk={monsterAtk} />
						</Slide>
	}

	renderListItems(encounter, index){
		if(!encounter.hasOwnProperty('coordinates')){
			return <div></div>
		}
		return <Slide direction='right' in={true} timeout={750}>
						<ListItem button key={`${encounter.coordinates.lat}:${encounter.coordinates.lng}`}
							onMouseEnter={event => {
	 		 				event.preventDefault();
		 		 				this.setState({
									activeItem: index
								})
							this.focusMapOn(encounter);
	 		 				}}>
								<Avatar>
									<ImageIcon />
								</Avatar>
								<ListItemText primary={`${encounter.enemy.name}`} secondary={`Position: lat ${encounter.coordinates.lat} long ${encounter.coordinates.lng}`} />
							</ListItem>
						</Slide>
	}




	render(){
		return (
				<Col key={COLUMN_KEY} xs={12} md={12} lg={12}>
					<Grid fluid>
						<div style={styles.encounterContent}>
							<Row style={styles.activeItem}>
								<Col xs={12} md={12} lg={12}>
										{this.renderActiveItem(this.props.encounters[this.state.activeItem])}
								</Col>
							</Row>
							<Row>
									<Col xs={12} md={12} lg={12}>
										<div style={styles.listRoot}>
											<List component='nav' style={styles.list}>
												{this.props.encounters.map((encounter, index) => this.renderListItems(encounter, index))}
											</List>
										</div>
									</Col>
							</Row>
						</div>
					</Grid>
				</Col>
			)
	}
}

function mapStateToProps(state){
	return {
		encounters:state.encounters
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({focusMapOnEncounter:focusMapOnEncounter}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(EncounterList);
