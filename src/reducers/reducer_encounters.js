import { ENCOUNTERS_FETCHED } from '../actions/index.js';

const DEFAULT_STATE = [{msg:'Fetch your encounters by pressing the button above...'}];

/**
*	Consider error handling upon broken requests etc.
*/
export default (state = DEFAULT_STATE, action) => {
	switch(action.type){
		case ENCOUNTERS_FETCHED:
			return action.payload.data.payload;
		default:
			return state;
	}
}
