import { combineReducers } from 'redux';

import ChangedMapSize from './reducer_map_size_changed.js';
import Encounters from './reducer_encounters.js';
import FocusedEncounter from './reducer_encounter_focused.js';

const rootReducer = combineReducers({
	changedMapSize:ChangedMapSize,
	encounters:Encounters,
	focusedEncounter:FocusedEncounter
});


export default rootReducer;
