import { ENCOUNTER_FOCUSED } from '../actions/index.js';


export default(state = null, action) => {
	if(action.payload != null){
		console.log(action.payload);
	}
	switch(action.type){
		case ENCOUNTER_FOCUSED:
			return action.payload;
		default:
			return state;
	}
	//return action.type === ENCOUNTER_FOCUSED ? action.payload : state;
}