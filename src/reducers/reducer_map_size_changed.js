import { MAP_SIZE_CHANGED } from '../actions/index.js';


export default (state = [], action) => {
	switch(action.type){
		case MAP_SIZE_CHANGED:
			return action.payload;
		default:
			return state;
	}
}