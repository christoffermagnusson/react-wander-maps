import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchEncounters } from '../actions/index.js';
import styles from '../../style/nav_bar_styles.css.js';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';


class NavBar extends Component{
	constructor(props){
		super(props);
		this.onFetchEncounters = this.onFetchEncounters.bind(this);

	}


	onFetchEncounters(event){
		console.log("Fetching encounters...");
		event.preventDefault();
		this.props.fetchEncounters();
	}

	render(){
		return (
					<div style={styles.root}>
						<AppBar position='static'>
								<Toolbar>
									<IconButton color='inherit' aria-label='Menu'>
										<Menu />
									</IconButton>
									<Typography variant='h5' color='inherit' style={styles.grow}>
										WanderMaps
									</Typography>
									<Link to='/'><Button color='inherit'>Home</Button></Link>
									<Link to='/map'><Button color='inherit'>Map</Button></Link>
									<Link to='/character'><Button color='inherit'>Character</Button></Link>
									<Button color='inherit' onClick={this.onFetchEncounters}>Generate</Button>
								</Toolbar>
						</AppBar>
					</div>
			)
	}
}


function mapDispatchToProps(dispatch){
	return bindActionCreators({
							fetchEncounters
							},
							dispatch);
}


export default connect(null, mapDispatchToProps)(NavBar);
