import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchEncounters } from '../../actions/index.js';

import { Grid, Row, Col } from 'react-flexbox-grid';

import MapDetail from '../../containers/map/map_detail.js';
import EncounterList from '../../containers/map/encounter_list.js';

import styles from '../../../style/map_view.css.js';

const MAP_KEY = 'MAP_KEY';

class MapView extends Component {
	constructor(props){
		super(props);

		this.onFetchEncounters = this.onFetchEncounters.bind(this);
	}




	onFetchEncounters(event){
		console.log("Fetching encounters...");
		event.preventDefault();
		this.props.fetchEncounters();
	}


	render(){
		return(
					<Grid fluid style={{marginTop: 10+'px'}}>
						<Row>
							<Col sm={12} md={4} lg={4}>
								<EncounterList />
							</Col>
							<Col sm={12} md={8} lg={8}>
								<MapDetail />
							</Col>
						</Row>
					</Grid>
			)
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
							fetchEncounters
							},
							dispatch);
}

export default connect(null, mapDispatchToProps)(MapView);
