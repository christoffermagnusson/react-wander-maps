import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import CharacterStats from './character_stats.js';
import CharacterSkills from './character_skills.js';
import { Tab, Tabs, Paper } from '@material-ui/core';


class CharacterView extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: 0
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event, value){
    event.preventDefault();
    console.log(`value : ${value}`);
    this.setState({
      value: value
    });
  }

  render(){
    return (
      <Grid fluid>
        <Row>
          <Col sm={12} md={12} lg={12}>
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor='primary'
                textColor='primary'
              >
                <Tab label='Stats' />
                <Tab label='Skills' />
                <Tab label='Misc' />
              </Tabs>
              {this.state.value === 0 ? <CharacterStats /> : <div />}
              {this.state.value === 1 ? <CharacterSkills /> : <div />}
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default CharacterView;
