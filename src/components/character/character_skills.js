import React, { Component } from 'react';
import { Paper, Typography, Button, Divider, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';


class CharacterSkills extends Component {
  constructor(props){
    super(props);
  }



  render(){
    let classes = this.props.classes;
    return (
      <div className={classes.root}>
        <Paper className={classes.content}>
          <Typography variant='h2'>
            Skills here
          </Typography>
          <Divider />
        </Paper>
      </div>
    )
  }
}

const styles = theme => ({
  root : {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 5,
    paddingTop: theme.spacing.unit,
    height: 100 + '%'
  },
  content: {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 3,
    height: 100 + '%'
  },
});


export default withStyles(styles)(CharacterSkills);
