import React, { Component } from 'react';
import { Paper, Typography, Button, Divider, Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';


/*
CharacterData - data model
  | attribute  | value
  | name       | string
  | stats      | Object or Array
  | skills     | Object or Array
  | class      | string
  | experience | Number
*/

class CharacterStats extends Component {
  constructor(props){
    super(props);
  }



  render(){
    let classes = this.props.classes;
    return (
      <div className={classes.root}>
        <Paper className={classes.content}>
          <Typography variant='h2'>
            Stats
          </Typography>
          <Divider />
          <Grid container spacing={24}>
            <Grid item xs={12} md={3} mdOffset={2} lg={3} lgOffset={2} className={classes.grid}>
              <img src='../../../resources/placeholder.png' className={classes.charImg} />
            </Grid>
            <Grid item xs={12} md={3} mdOffset={9} lg={3} lgOffset={9} className={classes.grid}>
              <Typography variant='h4' className={classes.gridItem}>
                Name : Christoffer
              </Typography>
              <Typography variant='h4' className={classes.gridItem}>
                Health : 200
              </Typography>
              <Typography variant='h4' className={classes.gridItem}>
                Class : Paladin
              </Typography>
              <Typography variant='h4' className={classes.gridItem}>
                Strength : 14
              </Typography>
              <Typography variant='h4' className={classes.gridItem}>
                Magic : 9
              </Typography>
              <Typography variant='h4' className={classes.gridItem}>
                Experience : 3450
              </Typography>
            </Grid>
            <Grid container spacing={24}>
              <Grid item xs={12} md={12} lg={12} className={classes.grid}>
                <Typography variant='h4' className={classes.gridItem}>
                  Consequatur rem veniam corrupti. Et enim dolorem architecto incidunt minus ut.
                  Debitis et omnis rerum perspiciatis aut aut exercitationem iusto.
                  Quia aperiam adipisci quo eligendi alias sint ullam. Voluptas animi
                  harum quam modi. Quia laborum dolor nulla eius suscipit.

                  Voluptatem enim eligendi illum quia. Vel sed doloribus quasi nihil sunt omnis aut.
                  Doloribus ea iusto eos occaecati. Impedit voluptatem veritatis voluptatem voluptas.
                  Velit impedit minus quo molestias quas aliquam et.

                  Est accusamus deleniti tenetur. Quia in ullam quis suscipit nisi. Qui minima praesentium vel.

                  Quo voluptatem praesentium neque. Debitis consequatur dolor sint et accusantium quo culpa.
                  Atque facilis omnis voluptate quis aut voluptatem. Iure fugit officia dicta numquam.
                  Ea qui consequatur sed eos non at quis.

                  Ut quo qui corrupti illo sit consequatur aliquam architecto. Enim ut nostrum nulla.
                  Voluptatem temporibus quia temporibus totam. Deleniti qui laborum porro.
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    )
  }
}

const styles = theme => ({
  root : {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 5,
    paddingTop: theme.spacing.unit,
    height: 100 + '%'
  },
  content: {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 3,
    height: 100 + '%'
  },
  grid: {
    marginTop: 20 + 'px'
  },
  gridItem: {
    marginTop: 10 + 'px'
  },
  charImg: {
    float: 'left'
  }
});


export default withStyles(styles)(CharacterStats);
