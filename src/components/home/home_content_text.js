import React, { Component } from 'react';
import { Paper, Typography, Divider, Grow } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const HomeContentText = (props) => {
  const { classes } = props;
  return (
      <div className={classes.root}>
        <Grow in={true} timeout={1000}>
          <Paper className={classes.content} elevation={1}>
              <Typography variant='h3'>
                News
              </Typography>
              <Divider />
              <Typography variant='body2'>
                Consequatur rem veniam corrupti. Et enim dolorem architecto incidunt minus ut.
                Debitis et omnis rerum perspiciatis aut aut exercitationem iusto.
                Quia aperiam adipisci quo eligendi alias sint ullam. Voluptas animi
                harum quam modi. Quia laborum dolor nulla eius suscipit.
              </Typography>
              <Typography variant='body2'>
                Voluptatem enim eligendi illum quia. Vel sed doloribus quasi nihil sunt omnis aut.
                Doloribus ea iusto eos occaecati. Impedit voluptatem veritatis voluptatem voluptas.
                Velit impedit minus quo molestias quas aliquam et.

                Est accusamus deleniti tenetur. Quia in ullam quis suscipit nisi. Qui minima praesentium vel.

                Quo voluptatem praesentium neque. Debitis consequatur dolor sint et accusantium quo culpa.
                Atque facilis omnis voluptate quis aut voluptatem. Iure fugit officia dicta numquam.
                Ea qui consequatur sed eos non at quis.

                Ut quo qui corrupti illo sit consequatur aliquam architecto. Enim ut nostrum nulla.
                Voluptatem temporibus quia temporibus totam. Deleniti qui laborum porro.
              </Typography>
              <img className={classes.img} src='../../resources/a-monster-calls-2.jpg' />
              <Divider />
              <Typography variant='body2'>
                Consequatur rem veniam corrupti. Et enim dolorem architecto incidunt minus ut.
                Debitis et omnis rerum perspiciatis aut aut exercitationem iusto.
                Quia aperiam adipisci quo eligendi alias sint ullam. Voluptas animi
                harum quam modi. Quia laborum dolor nulla eius suscipit.

                Voluptatem enim eligendi illum quia. Vel sed doloribus quasi nihil sunt omnis aut.
                Doloribus ea iusto eos occaecati. Impedit voluptatem veritatis voluptatem voluptas.
                Velit impedit minus quo molestias quas aliquam et.

                Est accusamus deleniti tenetur. Quia in ullam quis suscipit nisi. Qui minima praesentium vel.

                Quo voluptatem praesentium neque. Debitis consequatur dolor sint et accusantium quo culpa.
                Atque facilis omnis voluptate quis aut voluptatem. Iure fugit officia dicta numquam.
                Ea qui consequatur sed eos non at quis.

                Ut quo qui corrupti illo sit consequatur aliquam architecto. Enim ut nostrum nulla.
                Voluptatem temporibus quia temporibus totam. Deleniti qui laborum porro.
              </Typography>
           </Paper>
          </Grow>
        </div>
       );
}

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 5,
    paddingTop: theme.spacing.unit,
    height: 100 + '%'
  },
  content: {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 3,
    height: 100 + '%'
  },
  img: {
    ...theme.mixins.gutters(),
    paddingBottom: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 3,
    paddingLeft: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    width: 100 + '%',
    margin: 'auto'
  }
});

HomeContentText.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(HomeContentText);
