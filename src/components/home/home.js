import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import HomeContentText from './home_content_text.js';

class Home extends Component {
  constructor(props){
    super(props);
  }

  render(){
    return (
      <Grid fluid>
        <Row>
          <Col sm={12} mdOffset={3} md={6} lgOffset={3} lg={6}>
            <HomeContentText />
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default Home;
