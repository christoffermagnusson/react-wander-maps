import React, { Component } from "react";
import { Grid, Row } from 'react-flexbox-grid';

import MapView from './map/map_view.js';
import NavBar from './nav_bar.js';
import Routes from '../routes/routes.js'

export default class App extends Component{
	constructor(props){
		super(props)
	}


	render(){
		return(
			<div>
				<NavBar />
				<Routes />
			</div>
		)
	}
}
