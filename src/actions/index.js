import axios from 'axios';



const ENCOUNTER_BASE_URL = 'http://localhost:8082/encounter';



/**
* Contains the latest changed map size. Used for
* adjusting the MapMarker objects size.
*/

export const MAP_SIZE_CHANGED = 'MAP_SIZE_CHANGED';

export function changeMapSize(size){
	console.log(`map size changed to: ${size}`);
	return{
		type: MAP_SIZE_CHANGED,
		payload: size
	}
}


/**
*
*/

export const ENCOUNTERS_FETCHED = 'ENCOUNTERS_FETCHED';

export function fetchEncounters(){
	let queryUrl = `${ENCOUNTER_BASE_URL}/daily?username=Christoffer`;
	let encounters = axios.get(queryUrl);
	return{
		type: ENCOUNTERS_FETCHED,
		payload: encounters
	}
}

export const CHANGE_ENCOUNTER = 'CHANGE_ENCOUNTER';

export function changeEncounter(step){
	// do checks if 
}

export const ENCOUNTER_FOCUSED = 'ENCOUNTER_FOCUSED';

export function focusMapOnEncounter(encounter){
	return{
		type: ENCOUNTER_FOCUSED,
		payload: encounter
	}
}
