import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import MapView from '../components/map/map_view.js';
import CharacterView from '../components/character/character_view.js';
import Home from '../components/home/home.js';

const Routes = () => {
	return <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/character' component={CharacterView} />
					<Route path='/map' component={MapView} />
				</Switch>


}

export default Routes;
