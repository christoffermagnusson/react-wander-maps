import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';
import { BrowserRouter } from 'react-router-dom';

import reducers from './reducers';

const createStoreWithMiddleWare = applyMiddleware(ReduxPromise)(createStore);


import App from "./components/app";

ReactDOM.render(
	<Provider store={createStoreWithMiddleWare(reducers)}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>
	,document.querySelector(".root")
	);
