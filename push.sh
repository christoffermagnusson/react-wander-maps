COMMIT_MESSAGE=$1

if [ $# -eq 0 ]
    then
        echo "No commit message... try ./push.sh INSERT_MESSAGE_HERE"
        exit 1;
    else
        echo "Adding all changed files to stage..."
        git add -A
        echo "Commiting changes..."
        git commit -m "$COMMIT_MESSAGE"
        echo "Pushing changes to remote..."
        git push origin master
fi 
