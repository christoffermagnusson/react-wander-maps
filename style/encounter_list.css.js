const listRoot = {
	maxWidth: '100%',
	height: '70%'
}

const list = {
	overflow: 'auto',
	maxHeight: '100%'
}

const encounterContent = {
	width: '30%',
	maxHeight: '100%',
	position: 'absolute'
}

const activeItem = {
	height: '30%'
}


export default {
  encounterContent,
	listRoot,
	list,
	activeItem,
}
