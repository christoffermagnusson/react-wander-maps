const desc = {
  fontSize : '20px'
}

const card = {
  maxWidth : 100 + '%'
}

const img = {
  objectFit : 'cover'
}

export default {
  desc,
  card,
  img
}
