# react-wander-maps #
- - - -

### **Important** ###

*This repository represents an early version of my final vision with the project.  
It will be updated during development and so will this documentation.*

- - - -

### Outline ###

This repository contains frontend and UI code for my hobby project *wander*.  
*Wander* is a position based roleplaying game where players encounter monsters to defeat in the real world.  
This client is built with React.js framework using libraries such as Redux for state handling  
and Bootstrap for styling among others.




